<?php

namespace FirstCardBundle\Controller;

use FirstCardBundle\Entity\Transaction;
use FirstCardBundle\Service\PaymentForm;
use FirstCardBundle\Service\PaymentGateway;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package FirstCardBundle\Controller
 * @Route("/payment")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="fsc_default")
     */
    public function indexAction()
    {
        return new Response('-');
    }

    /**
     * @Route("/callback", name="fsc_callback")
     */
    public function callbackAction()
    {
        /** @var PaymentForm $form */
        $form = $this->get('fsc.form');
        $xml = $form->getResponse($_POST);


        /** @var Transaction $transaction */
        $transaction = $this->getDoctrine()
            ->getRepository('FirstCardBundle:Transaction')
            ->find($xml->transactionId);

        $transaction->setCallbackDate(new \DateTime());
        $transaction->setCallbackData(print_r($xml, 1));

        $em = $this->getDoctrine()->getManager();
        $em->persist($transaction);
        $em->flush();


        return new Response('callbackAction');
    }


    /**
     * @Route("/success_transaction", name="fsc_success_transaction")
     */
    public function successTransactionAction()
    {
//        return $this->redirect($this->generateUrl("fsc_form", false));

        return $this->render('FirstCardBundle:Default:success_transaction.html.twig');
    }

    /**
     * @Route("/success_transaction", name="fsc_fail_transaction")
     */
    public function failTransactionAction()
    {
        return $this->render('FirstCardBundle:Default:fail_transaction.html.twig');
    }

    /**
     * @Route("/success", name="fsc_success")
     */
    public function successAction()
    {
        /** @var PaymentForm $form */
        $form = $this->get('fsc.form');

        /** @var \SimpleXMLElement $xml */
        $xml = $form->getResponse($_POST);
        $em = $this->getDoctrine()->getManager();

        /** @var Transaction $transaction */
        $transaction = $this->getDoctrine()
            ->getRepository('FirstCardBundle:Transaction')
            ->find($xml->transactionId);

        $transaction->setResponseDate(new \DateTime());
        $transaction->setResponseData(print_r($xml, 1));
        $transaction->setResponseVerified(true);
        if ($xml->status == 'SUCCESS') {
            $transaction->setSuccess(true);
            $em->persist($transaction);
            $em->flush();
            if ($transaction->getLocale() == 'lv') {
                return $this->redirect('http://www.zoorbagan.com/payoklv');
            } else {
                return $this->redirect('http://www.zoorbagan.com/payokru');
            }
        }

        if ($transaction->getLocale() == 'lv') {
            return $this->redirect('http://www.zoorbagan.com/payfaillv');
        } else {
            return $this->redirect('http://www.zoorbagan.com/payfailru');
        }
    }

    /**
     * @Route("/redirect/{id}", name="fsc_redirect")
     */
    public function redirectAction(Transaction $transaction)
    {
        /** @var PaymentGateway $gateway */
        $gateway = $this->get('fsc.gateway');

        /** @var PaymentForm $form */
        $form = $this->get('fsc.form');
        $em = $this->getDoctrine()->getManager();

        $requestFields = array(
            'merchantId'        => $gateway->getMerchant(),
            'currency'          => 'EUR',
            'customerEmail'     => $transaction->getEmail(),
            'transactionId'     => $transaction->getId(),
            'orderId'           => $transaction->getId(),
            'orderDescription'  => "Zoorbagan nr. " . $transaction->getId(),
            'amount'            => $transaction->getTotal(),
            'successUrl'        => 'http://payment.zoorbagan.com' . $this->generateUrl("fsc_success", array(), false),
        );

        $transaction->setInitialRequest(print_r($requestFields, 1));
        $transaction->setCreatedDate(new \DateTime());
        $em->persist($transaction);
        $em->flush();

        $fields = $form->getRequest(
            $requestFields,
            '<data />'
        );

        return $this->render('FirstCardBundle:Default:redirect.html.twig', array(
            'fields' => $fields,
            'action' => $gateway->getActionUrl() . "process"
        ));
    }

    /**
     * @Route("/form", name="fsc_form")
     */
    public function formAction(Request $request)
    {
        $transaction  = new Transaction();
        $em = $this->getDoctrine()->getManager();
        $em->persist($transaction);
        $em->flush();

        $form = $this
            ->createFormBuilder($transaction, array('csrf_protection' => false))
            ->add('email', 'text')
            ->add('total', 'number')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transaction = $form->getData();

            $em->persist($transaction);
            $em->flush();
            ;
            return $this->redirect($this->generateUrl("fsc_redirect", array('id' => $transaction->getId()), false));
        }

        return $this->render('FirstCardBundle:Default:form.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/submit", name="fsc_form")
     */
    public function submitAction(Request $request)
    {
        $transaction  = new Transaction();

        if ($request->request->get('email', false) && $request->request->get('total', false)) {
            $transaction->setEmail($request->request->get('email', false));
            $transaction->setTotal($request->request->get('total', false));
            $transaction->setComment($request->request->get('comment', ''));
            $transaction->setLocale($request->request->get('locale', false));

            $em = $this->getDoctrine()->getManager();
            $em->persist($transaction);
            $em->flush();

            return $this->redirect($this->generateUrl("fsc_redirect", array('id' => $transaction->getId()), false));
        }

        return $this->redirect($this->generateUrl("fsc_form", false));
    }
}
