<?php
namespace FirstCardBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Created by PhpStorm.
 * User: nbi
 * Date: 15/01/2017
 * Time: 21:30
 */
class TransactionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('createdDate', 'datetime');
        $formMapper->add('email', 'text');
        $formMapper->add('total', 'text');
        $formMapper->add('comment', 'text');
        $formMapper->add('success');
        $formMapper->add('initialRequest');

        $formMapper->add('responseData');
        $formMapper->add('responseDate');
        $formMapper->add('responseVerified');

        $formMapper->add('callbackData');
        $formMapper->add('callbackDate');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');
        $datagridMapper->add('total');
        $datagridMapper->add('createdDate');
        $datagridMapper->add('success');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('createdDate');
        $listMapper->add('email');
        $listMapper->add('total');
        $listMapper->add('comment');
        $listMapper->add('success');
    }
}
