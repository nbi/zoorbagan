<?php

namespace FirstCardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="FirstCardBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $initialRequest;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $locale;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdDate;


    /**
     * @ORM\Column(type="float", precision=2, nullable=true)
     */
    protected $total;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $responseData;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $verificationData;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $callbackData;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $responseDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $verificationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $callbackDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $responseVerified = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $transactionVerified = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $successVerified = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $success = false;
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getInitialRequest()
    {
        return $this->initialRequest;
    }

    /**
     * @param mixed $initialRequest
     */
    public function setInitialRequest($initialRequest)
    {
        $this->initialRequest = $initialRequest;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getResponseData()
    {
        return $this->responseData;
    }

    /**
     * @param mixed $responseData
     */
    public function setResponseData($responseData)
    {
        $this->responseData = $responseData;
    }

    /**
     * @return mixed
     */
    public function getVerificationData()
    {
        return $this->verificationData;
    }

    /**
     * @param mixed $verificationData
     */
    public function setVerificationData($verificationData)
    {
        $this->verificationData = $verificationData;
    }

    /**
     * @return mixed
     */
    public function getCallbackData()
    {
        return $this->callbackData;
    }

    /**
     * @param mixed $callbackData
     */
    public function setCallbackData($callbackData)
    {
        $this->callbackData = $callbackData;
    }

    /**
     * @return mixed
     */
    public function getResponseDate()
    {
        return $this->responseDate;
    }

    /**
     * @param mixed $responseDate
     */
    public function setResponseDate($responseDate)
    {
        $this->responseDate = $responseDate;
    }

    /**
     * @return mixed
     */
    public function getVerificationDate()
    {
        return $this->verificationDate;
    }

    /**
     * @param mixed $verificationDate
     */
    public function setVerificationDate($verificationDate)
    {
        $this->verificationDate = $verificationDate;
    }

    /**
     * @return mixed
     */
    public function getCallbackDate()
    {
        return $this->callbackDate;
    }

    /**
     * @param mixed $callbackDate
     */
    public function setCallbackDate($callbackDate)
    {
        $this->callbackDate = $callbackDate;
    }

    /**
     * @return mixed
     */
    public function getResponseVerified()
    {
        return $this->responseVerified;
    }

    /**
     * @param mixed $responseVerified
     */
    public function setResponseVerified($responseVerified)
    {
        $this->responseVerified = $responseVerified;
    }

    /**
     * @return mixed
     */
    public function getTransactionVerified()
    {
        return $this->transactionVerified;
    }

    /**
     * @param mixed $transactionVerified
     */
    public function setTransactionVerified($transactionVerified)
    {
        $this->transactionVerified = $transactionVerified;
    }

    /**
     * @return mixed
     */
    public function getSuccessVerified()
    {
        return $this->successVerified;
    }

    /**
     * @param mixed $successVerified
     */
    public function setSuccessVerified($successVerified)
    {
        $this->successVerified = $successVerified;
    }

    /**
     * @return mixed
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param mixed $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
}
