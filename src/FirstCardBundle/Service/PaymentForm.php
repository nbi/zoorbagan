<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17/01/2017
 * Time: 16:16
 */
namespace FirstCardBundle\Service;

class PaymentForm {

    private static $required = array('KEY', 'REQUEST_DATA', 'SIGNATURE');

    private $_fSCPaymentGateway;

    public function __construct(PaymentGateway $FSCPaymentGateway) {
        $this->_fSCPaymentGateway = $FSCPaymentGateway;
    }

    /**
     * Returns array which should be POST'ed to FORMs URL
     * May throw Exception
     */
    public function getRequest($data, $fname) {

        $request = $this->_fSCPaymentGateway->prepare($data, $fname);
        return (array) $request;
    }

    /**
     * Takes POST array as an argument
     * May throw Exception
     */
    public function getResponse($response) {

        if (count(array_diff(self::$required, array_keys($response))) > 0) {
            throw new \Exception('Some of required POST params are not included!');
        }

        // Convert array to object
        $responseObj = new \stdClass();
        foreach ($response as $key => $value) {
            $responseObj->$key = $value;
        }

        return $this->_fSCPaymentGateway->parse($responseObj);
    }

}