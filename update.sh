#!/usr/bin/env bash
git pull
chmod +x update.sh
php app/console doctrine:migrations:migrate --no-interaction
php app/console assets:install --symlink web

composer dump-autoload --optimize
chmod -R 777 app/cache/
