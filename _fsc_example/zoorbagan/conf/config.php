<?php
$configs = array(
      'gateway_key'       => './conf/fsc-pubkey.pem', 
      'merchant_key'      => './conf/me-privkey.pem', 
      'action_base_url'   => 'https://test.firstswisscard.com/payments-new/fsc/merchant/', 
      'merchant_id'       => 'zoorbagan', 
      'merchant_email'    => 'ou@zoorbagan.com', // Change to test client email
      'merchant_currency' => 'EUR', 
      'complete_url'      => 'http://payment.zoorbagan.com/payment/complete', //URL where client will be redirected after payment
 ); 
