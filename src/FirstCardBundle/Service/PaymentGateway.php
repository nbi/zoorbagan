<?php
namespace FirstCardBundle\Service;

class PaymentGateway
{
    private $_merchantkey = null;
    private $_systemkey = null;

    private $merchant, $sysKeyPath, $merchantKeyPath, $actionUrl;

    public function __construct($merchant, $actionUrl, $sysKeyPath, $merchantKeyPath)
    {
        $this->merchant = $merchant;
        $this->actionUrl = $actionUrl;
        $this->sysKeyPath = $sysKeyPath;
        $this->merchantKeyPath = $merchantKeyPath;
        $this->loadKeys();
    }
//
//    public function __call($name, $arguments) {
//        return $this->exec($name, $arguments[0]);
//    }

    public function prepare($array_data, $fname)
    {

        if (!$fname) {
            $fname = '<data />';
        }
        $clean_data = $this->arrayToXml(new \SimpleXMLElement($fname), $array_data)->asXML();

        $sign = $this->sign($clean_data);
        $tmp = $this->encrypt($clean_data);

        /* Base64 encode everything */
        $sign = base64_encode($sign);
        $key = base64_encode($tmp['key']);
        $data = base64_encode($tmp['data']);

        /* Form request */
        $request = new \StdClass();
        $request->KEY = $key;
        $request->REQUEST_DATA = $data;
        $request->SIGNATURE = $sign;

        return $request;

    }

    public function parse($response)
    {

        $sign = base64_decode($response->SIGNATURE);
        $key = base64_decode($response->KEY);
        $data = base64_decode($response->REQUEST_DATA);

        $data = $this->decrypt($data, $key);

        if (!$this->checkSignature($data, $sign)) {
            throw new \Exception('Decryption failed, invalid signature!');
        }

        return simplexml_load_string($data);

    }

    private function sign($clean_data)
    {
        $merchantkeyid = openssl_get_privatekey($this->_merchantkey);
        $sign = null;

        if (!openssl_sign($clean_data, $sign, $merchantkeyid)) {
            throw new \Exception('Signing failed: ' . openssl_error_string());
        }
        openssl_free_key($merchantkeyid);

        return $sign;
    }

    private function checkSignature($data, $sign)
    {
        $systemkeyid = openssl_get_publickey($this->_systemkey);
        $res = (openssl_verify($data, $sign, $systemkeyid) == 1);
        openssl_free_key($systemkeyid);

        return $res;
    }

    private function encrypt($cleardata)
    {
        $systemkeyid = openssl_get_publickey($this->_systemkey);
        if (openssl_seal($cleardata, $data, $ekeys, array($systemkeyid))) {
            $key = $ekeys[0];
        } else {
            throw new \Exception('Encryption failed: ' . openssl_error_string());
        }
        openssl_free_key($systemkeyid);

        return array(
            'data' => $data,
            'key' => $key,
        );
    }

    private function decrypt($data, $key)
    {
        $merchantkeyid = openssl_get_privatekey($this->_merchantkey);
        if (!openssl_open($data, $cleardata, $key, $merchantkeyid)) {
            throw new \Exception('Decryption failed: ' . openssl_error_string());
        };
        openssl_free_key($merchantkeyid);

        return $cleardata;
    }

    public function loadKeys()
    {

        $m_keyfile = fopen($this->merchantKeyPath, 'r');
        $this->_merchantkey = fread($m_keyfile, filesize($this->merchantKeyPath));
        fclose($m_keyfile);

        $s_keyfile = fopen($this->sysKeyPath, 'r');
        $this->_systemkey = fread($s_keyfile, filesize($this->sysKeyPath));
        fclose($s_keyfile);

        unset($m_keyfile);
        unset($s_keyfile);
    }

    private function arrayToXml($xml, array $children)
    {
        foreach ($children as $name => $value) {
            (is_array($value)) ? $this->arrayToXml($xml->addChild($name), $value) : $xml->addChild($name, $value);
        }

        return $xml;
    }

    /**
     * @return mixed
     */
    public function getMerchant()
    {
        return $this->merchant;
    }

    /**
     * @param mixed $merchant
     */
    public function setMerchant($merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * @return mixed
     */
    public function getActionUrl()
    {
        return $this->actionUrl;
    }

    /**
     * @param mixed $actionUrl
     */
    public function setActionUrl($actionUrl)
    {
        $this->actionUrl = $actionUrl;
    }
}
