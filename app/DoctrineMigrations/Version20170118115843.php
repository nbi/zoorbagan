<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170118115843 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction ADD verification_data LONGTEXT DEFAULT NULL, ADD callback_data LONGTEXT DEFAULT NULL, ADD verification_date DATETIME DEFAULT NULL, ADD callback_date DATETIME DEFAULT NULL, ADD response_verified TINYINT(1) DEFAULT NULL, ADD transaction_verified TINYINT(1) DEFAULT NULL, ADD success_verified TINYINT(1) DEFAULT NULL, DROP get_verified, DROP post_verified, CHANGE get_request response_data LONGTEXT DEFAULT NULL, CHANGE get_date response_date DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction ADD get_request LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, ADD get_date DATETIME DEFAULT NULL, ADD get_verified TINYINT(1) DEFAULT NULL, ADD post_verified TINYINT(1) DEFAULT NULL, DROP response_data, DROP verification_data, DROP callback_data, DROP response_date, DROP verification_date, DROP callback_date, DROP response_verified, DROP transaction_verified, DROP success_verified');
    }
}
