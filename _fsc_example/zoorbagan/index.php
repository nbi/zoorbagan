<?php
require('./lib/fsc_lib.php');
require('./conf/config.php');

$fscPaymentGateway = new FSCPaymentGateway($configs);
$form = new FSCPaymentGatewayFORM($fscPaymentGateway);
?>


<?php
// ===============================
// ========== INIT FORM ==========
// ===============================
$fields = $form->getRequest(
    array(
        'merchantId'        => $configs['merchant_id'],
        'currency'          => $configs['merchant_currency'],
        'customerEmail'     => $configs['merchant_email'],//client's email; in test env. filled with merchant email
        'transactionId'     => rand(10000, 20000),
        'orderId'           => '15194',
        'orderDescription'  => "Example order",
        'amount'            => "10",
        'firstName'         => "John",
        'lastName'          => "Hill",
        'phone'             => "37121234567",
        'birthday'          => "05/24/1980",
        'successUrl'        => $configs['complete_url'],
    ),
    '<data />'
);

$action = $configs['action_base_url']."process";
?>
<h3>Init form:</h3>
<form action="<?php echo $action; ?>" method="post">
    <?php
    foreach ($fields as $key => $value) {
        echo "<div><label style='display:inline-block; width:140px;'>".$key."</label><input type='text' name='".$key."' value='".$value."' style='width: 600px;' /></div>\n";
    }
    ?>
    <input type="submit" name="SUBMIT" value="SUBMIT"/>
    <br>Submitting request to: <b><?php echo $action; ?></b><hr><br>
</form>





<?php
// ===============================
// ======== UNHOLD FORM ==========
// ===============================
$fields = $form->getRequest(
    array(
        'merchantId'        => $configs['merchant_id'],
        'transactionId'     => "12345",
    ),
    '<chargeData />'
);
$action = $configs['action_base_url']."unhold";
?>
<h3>HoldOff form:</h3>
<form action="<?php echo $action; ?>" method="post">
    <?php
    foreach ($fields as $key => $value) {
        echo "<div><label style='display:inline-block; width:140px;'>".$key."</label><input type='text' name='".$key."' value='".$value."' style='width: 600px;' /></div>\n";
    }
    ?>
    <input type="submit" name="SUBMIT" value="SUBMIT"/>
    <br>Submitting request to: <b><?php echo $action; ?></b><hr><br>
</form>





<?php
// ===============================
// ======== REVERS FORM ==========
// ===============================
$fields = $form->getRequest(
    array(
        'merchantId'        => $configs['merchant_id'],
        'transactionId'     => "12345",
    ),
    '<reverseData />'
);
$action = $configs['action_base_url']."revers";
?>
<h3>Reverse form:</h3>
<form action="<?php echo $action; ?>" method="post">
    <?php
    foreach ($fields as $key => $value) {
        echo "<div><label style='display:inline-block; width:140px;'>".$key."</label><input type='text' name='".$key."' value='".$value."' style='width: 600px;' /></div>\n";
    } ?>
    <input type="submit" name="SUBMIT" value="SUBMIT"/>
    <br>Submitting request to: <b><?php echo $action; ?></b><hr><br>
</form>

